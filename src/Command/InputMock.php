<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Command;

use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;

class InputMock implements InputInterface
{
    private array $arguments;
    private array $options;

    /**
     * @param string[] $arguments
     * @param string[] $options
     */
    public function __construct(array $arguments, array $options)
    {
        $this->arguments = $arguments;
        $this->options = $options;
    }

    public function getFirstArgument(): ?string
    {
        // TODO: Implement getFirstArgument() method.
    }

    public function hasParameterOption(array|string $values, bool $onlyParams = false): bool
    {
        // TODO: Implement hasParameterOption() method.
    }

    public function getParameterOption(array|string $values, float|array|bool|int|string|null $default = false, bool $onlyParams = false)
    {
        // TODO: Implement getParameterOption() method.
    }

    public function bind(InputDefinition $definition)
    {
        // TODO: Implement bind() method.
    }

    public function validate()
    {
        // TODO: Implement validate() method.
    }

    public function getArguments(): array
    {
       return $this->arguments;
    }

    public function getArgument(string $name)
    {
        // TODO: Implement getArgument() method.
    }

    public function setArgument(string $name, mixed $value)
    {
        // TODO: Implement setArgument() method.
    }

    public function hasArgument(string $name): bool
    {
        // TODO: Implement hasArgument() method.
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getOption(string $name)
    {
        // TODO: Implement getOption() method.
    }

    public function setOption(string $name, mixed $value)
    {
        // TODO: Implement setOption() method.
    }

    public function hasOption(string $name): bool
    {
        // TODO: Implement hasOption() method.
    }

    public function isInteractive(): bool
    {
        // TODO: Implement isInteractive() method.
    }

    public function setInteractive(bool $interactive)
    {
        // TODO: Implement setInteractive() method.
    }
}