<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\HttpClient;

use BadMethodCallException;
use Closure;
use PHPUnit\Framework\Assert;
use stdClass;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\ResponseStreamInterface;

class HttpClientMock implements HttpClientInterface
{
    private int $requestNum = 1;
    /**
     * @var string[]
     */
    private array $methods;
    /**
     * @var string[]
     */
    private array $urls = [];
    /**
     * @var array[]
     */
    private array $options;
    /**
     * @var TransportExceptionInterface[]
     */
    private array $exceptions;
    /**
     * @var ResponseMock[]
     */
    private array $responses;

    public function setUpResponse(ResponseMock $response, int $requestNum = 1): void
    {
        $this->responses[$requestNum] = $response;
    }
    public function setUpException(TransportExceptionInterface $exception, int $requestNum = 1): void
    {
        $this->exceptions[$requestNum] = $exception;
    }

    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        $this->methods[$this->requestNum] = $method;
        $this->urls[$this->requestNum] = $url;
        $this->options[$this->requestNum] = $options;

        if (isset($this->exceptions[$this->requestNum])) {
            throw $this->exceptions[$this->requestNum];
        }

        if (! isset($this->responses[$this->requestNum])) {
            $this->responses[$this->requestNum]  = new ResponseMock();
        }

        $this->responses[$this->requestNum]->setMethod($method);
        $this->responses[$this->requestNum]->setUrl($url);

        return $this->responses[$this->requestNum++];
    }

    public function expectNoRequestHasBeenMade(): void
    {
        Assert::assertEmpty($this->urls);
    }

    public function expectRequestToHaveBeenMadeTo(string $url, int $requestNum = 1): void
    {
        Assert::assertEquals($url, $this->urls[$requestNum]);
    }

    public function expectRequestToHaveBeenMadeWithMethod(string $method, int $requestNum = 1): void
    {
        Assert::assertEquals($method, $this->methods[$requestNum]);
    }

    public function expectRequestToHaveBeenMadeWithHeader(string $key, string $value, int $requestNum = 1): void
    {
        Assert::assertArrayHasKey($key, $this->options[$requestNum]['headers']);
        Assert::assertEquals($value, $this->options[$requestNum]['headers'][$key]);
    }

    public function expectRequestToHaveBeenMadeWithBody(array|string|Closure $json, int $requestNum = 1): void
    {
        Assert::assertEquals($json, $this->options[$requestNum]['body']);
    }

    public function expectRequestToHaveBeenMadeWithJSON(array|stdClass $json, int $requestNum = 1): void
    {
        Assert::assertEquals($json, $this->options[$requestNum]['json']);
    }

    public function expectRequestToHaveBeenMadeWithRegex(array|stdClass $json, int $requestNum = 1): void
    {
        $actualJson = $this->options[$requestNum]['json'];
        $this->validateWithRegex($json, $actualJson);
    }

    private function validateWithRegex($expected, $actual): void
    {
        if (is_array($expected)) {
            $this->handleArray($actual, $expected);
        }

        elseif (is_object($expected)) {
            $this->handleObject($actual, $expected);
        }

        elseif (is_string($expected) && @preg_match($expected, '') !== false) {
            Assert::assertMatchesRegularExpression($expected, $actual);
        }

        else {
            Assert::assertEquals($expected, $actual);
        }
    }

    public function handleArray($actual, array $expected): void
    {
        Assert::assertIsArray($actual);  // Ensure the actual value is also an array
        foreach ($expected as $key => $value) {
            if (is_int($key)) {
                // If the key is an integer, it might be an array of objects/arrays
                Assert::assertArrayHasKey($key, $actual);
                $this->validateWithRegex($value, $actual[$key]);
            } else {
                // Otherwise, validate normally with the key
                Assert::assertArrayHasKey($key, $actual);
                $this->validateWithRegex($value, $actual[$key]);
            }
        }
    }

    public function handleObject($actual, $expected): void
    {
        Assert::assertIsObject($actual);  // Ensure the actual value is also an object
        foreach ($expected as $key => $value) {
            Assert::assertObjectHasProperty($key, $actual);
            $this->validateWithRegex($value, $actual->$key);
        }
    }

    public function expectRequestToHaveBeenMadeWithAuthBearerToken(string $token, int $requestNum = 1): void
    {
        Assert::assertEquals($token, $this->options[$requestNum]['auth_bearer']);
    }

    public function expectRequestNeverHappened(int $requestNum): void
    {
        Assert::assertFalse(isset($this->options[$requestNum]));
    }

    public function expectAmountOfRequestsHaveBeenMade(int $requestAmount): void
    {
        Assert::assertEquals($requestAmount, count($this->responses));
    }

    public function stream(iterable|ResponseInterface $responses, float $timeout = null): ResponseStreamInterface
    {
        throw new BadMethodCallException('The stream method is not available in this mock');
    }

    public function withOptions(array $options): static
    {
        throw new BadMethodCallException('The withOptions method is not available in this mock');
    }
}