<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\HttpClient;

use BadMethodCallException;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\RedirectionException;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ResponseMock implements ResponseInterface
{
    private int $statusCode;
    private string $method;
    private array $headers;
    private string $content;
    private string $url;

    public function __construct(int $statusCode = Response::HTTP_OK, array $headers = [], string $content = '')
    {
        $this->statusCode = $statusCode;
        $this->headers = $headers;
        $this->content = $content;
    }

    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function getStatusCode(): int
    {
        if ($this->statusCode === 0) {
            throw new TransportException('Transport exception.');
        }

        return $this->statusCode;
    }

    public function getHeaders(bool $throw = true): array
    {
        $this->throwExceptions($throw);
        return $this->headers;
    }

    public function getContent(bool $throw = true): string
    {
        $this->throwExceptions($throw);
        return $this->content;
    }

    private function throwExceptions(bool $throw)
    {
        if (! $throw) {
            return;
        }

        if ($this->statusCode === 0) {
            throw new TransportException();
        } else if ($this->statusCode >= 300 && $this->statusCode < 400) {
            throw new RedirectionException($this);
        } else if ($this->statusCode >= 400 && $this->statusCode < 500) {
            throw new ClientException($this);
        } else if ($this->statusCode >= 500 && $this->statusCode < 600) {
            throw new ServerException($this);
        }
    }

    public function toArray(bool $throw = true): array
    {
        throw new BadMethodCallException('The toArray method is not available in this mock');
    }

    public function cancel(): void
    {
        throw new BadMethodCallException('The cancel method is not available in this mock');
    }

    /**
     * @see ResponseInterface::getInfo
     */
    public function getInfo(string $type = null): mixed
    {
        $info = [
            "canceled" => true,
            "http_code" => $this->statusCode,
            "http_method" => $this->method,
            "redirect_count" => 0,
            "redirect_url" => null,
            "response_headers" => $this->headers,
            "start_time" => time(),
            "url" => $this->url,
            "user_data" => null
        ];

        return $info[$type];
    }
}