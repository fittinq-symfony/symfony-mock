<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\HttpClient;

use stdClass;
use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Request;

class RequestMock extends Request
{
    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct(stdClass|array $content)
    {
        $this->content = json_encode($content);
    }

    /**
     * @param HeaderBag $headers
     */
    public function setHeaders(HeaderBag $headers): void
    {
        $this->headers = $headers;
    }
}