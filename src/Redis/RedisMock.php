<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Redis;

use PHPUnit\Framework\Assert;
use Predis\Response\Status;
use stdClass;
use SymfonyBundles\RedisBundle\Redis\Client;

class RedisMock extends Client
{
    private array $stored = [];
    public function get(string $key): null|string
    {
        $object = $this->stored[$key] ?? null;
        return $object?->value;
    }

    public function set(string $key, $value, $expireResolution = null, $expireTTL = null, $flag = null): Status
    {
        $object = new stdClass();
        $object->value = $value;
        $object->expireResolution = $expireResolution;
        $object->expireTTL = $expireTTL;
        $object->flag = $flag;

        $this->stored[$key] = $object;

        return new Status('');
    }

    public function assertValueStoredInRedis(string $key, $value, $expireResolution = null, $expireTTL = null, $flag = null): void
    {
        $object = $this->stored[$key];
        Assert::assertNotEmpty($object);
        Assert::assertEquals($value, $object->value);
        Assert::assertEquals($expireResolution, $object->expireResolution);
        Assert::assertEquals($expireTTL, $object->expireTTL);
        Assert::assertEquals($flag, $object->flag);
    }
}