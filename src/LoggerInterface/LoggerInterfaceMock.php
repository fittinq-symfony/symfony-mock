<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\LoggerInterface;

use PHPUnit\Framework\Assert;
use Psr\Log\LoggerInterface;
use Stringable;

class LoggerInterfaceMock implements LoggerInterface
{
    private string $message;
    private array $context = [];

    public function warning($message, array $context = []): void
    {
        $this->message = $message;
        $this->context = $context;
    }

    public function emergency($message, array $context = array()): void
    {
        $this->message = $message;
        $this->context = $context;
    }

    public function alert($message, array $context = array()): void
    {
        $this->message = $message;
        $this->context = $context;
    }

    public function critical($message, array $context = array()): void
    {
        $this->message = $message;
        $this->context = $context;
    }

    public function error($message, array $context = array()): void
    {
        $this->message = $message;
        $this->context = $context;
    }

    public function notice($message, array $context = array()): void
    {
        $this->message = $message;
        $this->context = $context;
    }

    public function info($message, array $context = array()): void
    {
        $this->message = $message;
        $this->context = $context;
    }

    public function debug($message, array $context = array()): void
    {
        $this->message = $message;
        $this->context = $context;
    }

    public function log($level, $message, array $context = array()): void
    {
        $this->message = $message;
        $this->context = $context;
    }

    public function assertMessageHasBeenLogged(string $message, array $context = []): void
    {
        Assert::assertStringContainsString($message, $this->message);
        Assert::assertEquals($context, $this->context);
    }
}