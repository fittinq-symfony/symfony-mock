<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Environment;

use PHPUnit\Framework\Assert;
use Twig\Environment;

class EnvironmentMock extends Environment
{
    private array $views;
    private array $parameters;
    private int $requestNumber = 1;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct(){}

    public function render($name, array $context = []): string
    {
        $this->views[$this->requestNumber] = $name;
        $this->parameters[$this->requestNumber++] = $context;

        return '';
    }

    public function expectViewToBeRendered(string $expectedView, int $requestNumber = 1): void
    {
        Assert::assertEquals($expectedView, $this->views[$requestNumber], "$expectedView has not been rendered");
    }

    public function expectParametersToBePassedToView(array $expectedParameters, int $requestNumber = 1): void
    {
        Assert::assertEquals($expectedParameters, $this->parameters[$requestNumber], "Parameters have not been passed to template");
    }
}