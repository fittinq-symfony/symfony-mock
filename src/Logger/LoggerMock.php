<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Logger;

use PHPUnit\Framework\Assert;
use Psr\Log\AbstractLogger;
use Stringable;
use Throwable;

class LoggerMock extends AbstractLogger
{
    protected array $logLevels = [];
    protected array $messages = [];
    protected array $contexts = [];
    private ?Throwable $throwable = null;

    public function setUpWithException(Throwable $throwable): void
    {
        $this->throwable = $throwable;
    }

    /**
     * @throws Throwable
     */
    public function log($level, string|Stringable $message, array $context = []): void
    {
        $this->logLevels[] = $level;
        $this->messages[] = $message;
        $this->contexts[] = $context;

        if ($this->throwable) {
            throw $this->throwable;
        }
    }

    public function expectNotToLogAnything(): void
    {
        Assert::assertEmpty($this->messages, 'Expected not to write a log, but did actually log something');
    }

    public function expectToHaveLoggedLast(mixed $loglevel, string $message): void
    {
        Assert::assertNotEmpty($this->logLevels);
        Assert::assertNotEmpty($this->messages);

        $i = count ($this->messages) - 1;

        Assert::assertEquals($loglevel, $this->logLevels[$i]);
        Assert::assertEquals($message, $this->messages[$i]);
    }

    /**
     * @param mixed $loglevel
     * @param string $message
     * @param int $i We had different solutions here, but passing the index is needed to have better messages when
     * the assertion fails. We should be able to predict what the index of the log is anyway, since the contexts are
     * simple enough.
     */
    public function expectToHaveLoggedAtIndex(mixed $loglevel, string $message, int $i): void
    {
        Assert::assertEquals($loglevel, $this->logLevels[$i]);
        Assert::assertEquals($message, $this->messages[$i]);
    }
}