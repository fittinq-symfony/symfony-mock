<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Cache;

use DateTime;
use DateTimeInterface;
use InvalidArgumentException;
use PHPUnit\Framework\Assert;
use Psr\Cache\CacheItemInterface;
use Symfony\Contracts\Cache\ItemInterface;

class CacheItemMock implements CacheItemInterface, ItemInterface
{
    private string $key;
    private mixed $value;
    private array $metadata = [];

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function get(): mixed
    {
        return $this->value;
    }

    public function isHit(): bool
    {
        return false;
    }

    public function set(mixed $value): static
    {
        $this->value = $value;
        return $this;
    }

    public function expectToExpireAt(float $timestamp) {
        Assert::assertEquals($this->metadata[self::METADATA_EXPIRY], $timestamp);
    }

    public function expiresAt(?DateTimeInterface $expiration): static
    {
        $this->metadata[self::METADATA_EXPIRY] = $expiration !== null ? (float) $expiration->format('U.u') : null;
        return $this;
    }

    public function expiresAfter(mixed $time): static
    {
        if (null === $time) {
            $this->metadata[self::METADATA_EXPIRY] = null;
        } elseif ($time instanceof \DateInterval) {
            $this->metadata[self::METADATA_EXPIRY] = microtime(true) + (float) (new DateTime())->add($time)->format('U.u');
        } elseif (\is_int($time)) {
            $this->metadata[self::METADATA_EXPIRY] = $time + microtime(true);
        } else {
            throw new InvalidArgumentException(sprintf('Expiration date must be an integer, a DateInterval or null, "%s" given.', get_debug_type($time)));
        }

        return $this;
    }

    public function tag(iterable|string $tags): static
    {
        return $this;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }
}