<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Cache;

use DateTimeInterface;
use PHPUnit\Framework\Assert;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class CacheMock implements CacheInterface
{
    /**
     * @var CacheItemMock[]
     */
    private array $cache = [];

    public function setUpCache(string $key, mixed $value, DateTimeInterface $expiresAt)
    {
        $item = new CacheItemMock($key);
        $item->set($value);
        $item->expiresAt($expiresAt);
        $this->cache[$key] = $item;
    }

    public function get(string $key, callable $callback, float $beta = null, array &$metadata = null): mixed
    {
        if (!isset($this->cache[$key]) || $this->cache[$key]->getMetadata()[ItemInterface::METADATA_EXPIRY] < time()) {
            $item = new CacheItemMock($key);
            $item->set($callback($item, true));

            if (isset($metadata[ItemInterface::METADATA_EXPIRY])) {
                $item->expiresAt($metadata[ItemInterface::METADATA_EXPIRY]);
            }

            $this->cache[$key] = $item;
        }
        return $this->cache[$key]->get();
    }

    public function getItem($key): ?CacheItemMock
    {
        return $this->cache[$key] ?? null;
    }

    public function delete(string $key): bool
    {
        if (isset($this->cache[$key])) {
            unset($this->cache[$key]);
            return true;
        }

        return false;
    }

    public function expectCacheToHaveCacheItem(string $key, mixed $value)
    {
        Assert::assertEquals($this->cache[$key]->get(), $value);
    }

    public function expectCacheToNotHaveCacheItem(string $key)
    {
        Assert::assertNotContains($key, $this->cache);
    }
}