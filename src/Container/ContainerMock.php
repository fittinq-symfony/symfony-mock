<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Container;

use Psr\Container\ContainerInterface;

class ContainerMock implements ContainerInterface
{
    private array $entries = [];

    public function add(string $id, mixed $entry)
    {
        $this->entries[$id] = $entry;
    }

    public function get(string $id)
    {
        return $this->entries[$id] ?? null;
    }

    public function has(string $id): bool
    {
        return isset($this->entries[$id]);
    }
}
