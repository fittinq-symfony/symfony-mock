<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Doctrine;

use Doctrine\DBAL\Connection;

class ConnectionMock extends Connection
{
    private bool $transactionIsActive = false;
    private bool $isConnected = true;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct() {
    }


    public function connect(): \Doctrine\DBAL\Driver\Connection
    {
        return new DriverConnectionMock();
    }

    public function close(): void
    {

    }

    public function isConnected(): bool
    {
        return $this->isConnected;
    }

    public function isTransactionActive(): bool
    {
        return $this->transactionIsActive;
    }

    public function activateTransaction(): void
    {
        $this->transactionIsActive = true;
    }
}