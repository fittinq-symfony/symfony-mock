<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Doctrine;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\Assert;

class ManagerRegistryMock implements ManagerRegistry
{
    private EntityManagerMock $entityManager;
    private bool $isReset = false;

    public function __construct(EntityManagerMock $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getDefaultConnectionName(): string
    {
        // TODO: Implement getDefaultConnectionName() method.
    }

    public function getConnection(?string $name = null): object
    {
        // TODO: Implement getConnection() method.
    }

    public function getConnections(): array
    {
        // TODO: Implement getConnections() method.
    }

    public function getConnectionNames(): array
    {
        // TODO: Implement getConnectionNames() method.
    }

    public function getDefaultManagerName(): string
    {
        // TODO: Implement getDefaultManagerName() method.
    }

    public function getManagers(): array
    {
        // TODO: Implement getManagers() method.
    }

    public function resetManager(?string $name = null): ObjectManager
    {
        $this->isReset = true;
        return $this->entityManager;
    }

    public function getManagerNames(): array
    {
        // TODO: Implement getManagerNames() method.
    }

    public function getRepository(string $persistentObject, ?string $persistentManagerName = null): ObjectRepository
    {
        // TODO: Implement getRepository() method.
    }

    public function getManagerForClass(string $class): ?ObjectManager
    {
        // TODO: Implement getManagerForClass() method.
    }

    public function getManager(?string $name = null): EntityManagerMock
    {
        return $this->entityManager;
    }

    public function assertManagerIsReset(): void
    {
        Assert::assertTrue($this->isReset);
    }
}