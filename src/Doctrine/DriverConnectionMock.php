<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Doctrine;

use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\Driver\Result;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\DBAL\ParameterType;

class DriverConnectionMock implements Connection
{
    public function prepare(string $sql): Statement
    {
        // TODO: Implement prepare() method.
    }

    public function query(string $sql): Result
    {
        // TODO: Implement query() method.
    }

    public function quote($value, $type = ParameterType::STRING): string
    {
        // TODO: Implement quote() method.
    }

    public function exec(string $sql): int
    {
        // TODO: Implement exec() method.
    }

    public function lastInsertId($name = null)
    {
        // TODO: Implement lastInsertId() method.
    }

    public function beginTransaction(): void
    {
        // TODO: Implement beginTransaction() method.
    }

    public function commit(): void
    {
        // TODO: Implement commit() method.
    }

    public function rollBack(): void
    {
        // TODO: Implement rollBack() method.
    }

    public function getNativeConnection()
    {
        // TODO: Implement getNativeConnection() method.
    }

    public function getServerVersion(): string
    {
        // TODO: Implement getServerVersion() method.
    }
}