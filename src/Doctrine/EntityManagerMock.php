<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Mock\Doctrine;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\Assert;
use ReflectionException;
use ReflectionObject;
use Throwable;

class EntityManagerMock extends EntityManager
{
    private Connection $connection;
    private array $persisted;
    private array $flushed;
    private bool $isCommitted = false;
    private bool $isRolledBack = false;
    private bool $transactionStarted = false;
    private bool $transactionRequestStarted = false;
    private bool $isCleared = false;
    private bool $isOpen = true;
    private static int $AUTO_INCREMENT = 0;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
        $this->connection = new ConnectionMock();
        $this->persisted = [];
        $this->flushed = [];
    }

    /**
     * Custom logic for getting persisted entities
     */
    public function getPersistedEntities(string $className): array
    {
        if (isset($this->persisted[$className])) {
            return $this->persisted[$className];
        }

        return [];
    }

    /**
     * Custom logic for getting flushed entities
     */
    public function getFlushedEntities(string $className): array
    {
        if (isset($this->flushed[$className])) {
            return $this->flushed[$className];
        }

        return [];
    }

    /**
     * Custom logic for getting flushed entities
     */
    public function getCommittedEntities(string $className): array
    {
        if (isset($this->committed[$className])) {
            return $this->committed[$className];
        }

        return [];
    }

    /**
     * Custom logic for persisting entities
     * @throws ReflectionException
     */
    public function persist($entity): void
    {
        // Check if the entity already has an ID
        if ($entity->getId() !== null && isset($this->persisted[get_class($entity)][$entity->getId()])) {
            $this->persisted[get_class($entity)][$entity->getId()] = $entity;
        } else {
            // Dynamically set the ID using reflection
            $reflection = new ReflectionObject($entity);
            $idProperty = $reflection->getProperty('id');
            $idProperty->setAccessible(true);
            $idProperty->setValue($entity, self::$AUTO_INCREMENT++);

            $this->persisted[get_class($entity)][$entity->getId()] = $entity;
        }
    }

    /**
     * Custom logic for removing entities
     */
    public function remove($entity): void
    {
        unset($this->persisted[get_class($entity)][$entity->getId()]);
    }

    /**
     * Custom logic for flushing entities
     */
    public function flush($entity = null): void
    {
        $this->flushed = $this->persisted;
    }

    public function beginTransaction(): void
    {
        $this->connection->activateTransaction();
        $this->transactionStarted = true;
    }

    /**
     * @throws Throwable
     */
    public function transactional($func): mixed
    {
        $this->transactionRequestStarted = true;

        $this->beginTransaction();
        try {
            $res = $func();
            $this->commit();

            return $res;
        } catch (Throwable $e) {
            $this->rollBack();

            throw $e;
        }
    }

    public function assertTransactionStarted(): void
    {
        Assert::assertTrue($this->transactionStarted);
    }

    public function commit(): void
    {
        $this->isCommitted = true;
    }

    public function assertDataIsCommitted(): void
    {
        Assert::assertTrue($this->isCommitted);
    }

    public function rollback(): void
    {
        $this->isRolledBack = true;
    }

    public function assertDataMutationsAreRolledBack(): void
    {
        Assert::assertTrue($this->isRolledBack);
    }

    public function clear($entityName = null): void
    {
        $this->isCleared = true;
    }

    public function assertUnitOfWorkIsCleared(): void
    {
        Assert::assertTrue($this->isCleared);
    }

    public function close(): void
    {
        $this->isOpen = false;
    }

    public function isOpen(): bool
    {
        return $this->isOpen;
    }

    public function getConnection(): Connection
    {
        return $this->connection;
    }
}