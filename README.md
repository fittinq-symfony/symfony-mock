# Symfony Mock

The symfony mock bundle provides multiple mocks for unit testing applications. 
The following are mocks are included:

### Cache
CacheItemMock <br>
CacheMock

### Command
InputMock

### Container
ContainerMock

### Environment
EnvironmentMock

### HttpClient
HttpClientMock <br>
ResponseMock

### Logger
LoggerMock

### LoggerInterface
LoggerInterfaceMock